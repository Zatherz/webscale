--[[
  Copyright 2016 Dominik "Zatherz" Banaszak [zatherz at linux dot pl]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
--]]
local window = am.window({title = "Web Scaling Service", width = 400, height = 300})
math.randomseed(os.time())
window.scene = am.group()
local scale = 0.3
-- Image from:
-- http://www.public-domain-image.com/full-image/fauna-animals-public-domain-images-pictures/insects-and-bugs-public-domain-images-pictures/spiders-pictures/spider-web.jpg.html
window.scene:append(
        am.scale(1, 1):action(function(node)
                scale = scale - am.delta_time
                if scale <= 0 then
                        scale = 0.3
                        node.scale2d = vec2(math.random(5, 10)/10, math.random(5, 10)/10)
                end
        end)
        ^ am.sprite("web.jpg")
)
